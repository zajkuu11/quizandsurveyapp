package zajkuu.quizandsurveyapp.service;

import zajkuu.quizandsurveyapp.entity.Question;

import java.util.List;

public interface QuestionService {
    List<Question> getQuestionList();

    Question getQuestion(Long id);

    Question save(Question question);

    void deleteQuestion(Long id);
}
