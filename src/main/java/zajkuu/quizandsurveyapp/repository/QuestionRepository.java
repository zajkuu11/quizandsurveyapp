package zajkuu.quizandsurveyapp.repository;

import zajkuu.quizandsurveyapp.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}
