package zajkuu.quizandsurveyapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuizandsurveyappApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuizandsurveyappApplication.class, args);
    }

}

