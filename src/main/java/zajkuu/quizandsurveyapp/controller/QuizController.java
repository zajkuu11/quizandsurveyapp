package zajkuu.quizandsurveyapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import zajkuu.quizandsurveyapp.entity.Question;
import zajkuu.quizandsurveyapp.entity.Quiz;
import zajkuu.quizandsurveyapp.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.List;


@Controller
@RequestMapping({"/quizzes"})
public class QuizController {
    private Iterator<Question> questionIterator;

    private Quiz currentQuiz;

    private Quiz getCurrentQuiz() {
        return currentQuiz;
    }

    private void setCurrentQuiz(Quiz currentQuiz) {
        this.currentQuiz = currentQuiz;
    }

    private QuizService quizService;

    @Autowired
    public void setQuizService(QuizService quizService) {
        this.quizService = quizService;
    }

    @PostMapping
    public Quiz create(@RequestBody Quiz quiz) {
        return quizService.save(quiz);
    }

    @GetMapping(path = {"/{id}"})
    public String getQuiz(@PathVariable("id") Long id, Model model) {
        Quiz selectedQuiz = quizService.getQuiz(id);
        setCurrentQuiz(selectedQuiz);
        model.addAttribute("quizToSolve", selectedQuiz);
        return "quiz";
    }

    @PutMapping
    public Quiz update(@RequestBody Quiz quiz) {
        return quizService.save(quiz);
    }

    @DeleteMapping(path = {"/quiz/delete/{id}"})
    public void delete(@PathVariable("id") Long id) {
        quizService.deleteQuiz(id);
    }

    @GetMapping(path = "")
    public String findAllQuizzes(Model model) {
        List<Quiz> quizList = quizService.qetQuizList();
        model.addAttribute("quizzes", quizList);
        return "quizzes";
    }

    @GetMapping(path = {"/solve"})
    public String solveQuiz(Model model) {
        Question question;
        List<Question> questions = getCurrentQuiz().getQuestions();
        questionIterator = questions.iterator();
        if (questionIterator.hasNext()) {
            question = questionIterator.next();
            model.addAttribute("currentQuestion", question);
            return "quizSolve";
        }
        else return "/";
    }
}
