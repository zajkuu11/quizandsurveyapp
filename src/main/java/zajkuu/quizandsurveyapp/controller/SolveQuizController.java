package zajkuu.quizandsurveyapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import zajkuu.quizandsurveyapp.entity.Question;
import zajkuu.quizandsurveyapp.entity.Quiz;
import zajkuu.quizandsurveyapp.service.QuizService;

import java.util.Iterator;
import java.util.List;

@Controller
@RequestMapping("/solve")
public class SolveQuizController {

    private QuizService quizService;

    @Autowired
    public void setQuizService(QuizService quizService) {
        this.quizService = quizService;
    }

    @GetMapping(path = "/{id}")
    public String solveQuiz(@PathVariable("id") Long id, Model model) {
        Question currentQuestion;
        Quiz quizToSolve = quizService.getQuiz(id);
        List<Question> questions = quizToSolve.getQuestions();
        Iterator<Question> questionIterator = questions.iterator();
        if (questionIterator.hasNext()) {
            currentQuestion = questionIterator.next();
            model.addAttribute("currentQuestion", currentQuestion);
            return "quizSolve";
        } else
            return "index";
    }
}
